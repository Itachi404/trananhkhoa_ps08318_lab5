<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
class VC extends Controller
{

    public function create()
    {
        return view('register');
    }

    public function store(Request $request)
    {

        $request->validate([
            'user' => 'required',
            'pass' => 'required',
            'phone' => 'required|numeric|in_phone|min:10|max:10',
            'cmnd' => 'required|numeric|in_number',
            'birthday' => 'required|numeric|in_birthday',
        ]);
    }


}
