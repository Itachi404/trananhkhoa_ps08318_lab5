<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Đăng nhập</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <div class="container mt-5">
        <form action="" method="POST">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">Username</label>
                <input type="text" name="user" class="form-control" id="exampleInputEmail1" placeholder="Username">
                @error('user')
                    <div class="mt-2 alert alert-danger">{{ $message }}</div>
                @enderror
                @if($admin ?? 0 != '')
                    <div class="mt-2 alert alert-danger">{{ $admin ?? '' }}</div>
                @endif
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" name="pass" class="form-control" id="exampleInputPassword1" placeholder="Password">
                @error('pass')
                    <div class="mt-2 alert alert-danger">{{ $message }}</div>
                @enderror
                @if($admin ?? 0 != '')
                    <div class="mt-2 alert alert-danger">{{ $admin ?? '' }}</div>
                @endif
            </div>
            <button type="submit" class="btn btn-primary">Đăng nhập</button>
            <a class="btn btn-primary" href="{{'dangky'}}">Đăng ký</a>
        </form>
    </div>
</body>
</html>
