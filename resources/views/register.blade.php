<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Đăng ký</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
       <div class="container mt-5">
            <form action="" method="POST">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                    <label for="inputEmail4">Username</label>
                    <input type="text" name="user" class="form-control" id="inputEmail4" placeholder="Username">
                    @error('user')
                        <div class="mt-2 alert alert-danger">{{ $message }}</div>
                    @enderror
                    </div>
                    <div class="form-group col-md-6">
                    <label for="inputPassword4">Password</label>
                    <input type="password" name="pass" class="form-control" id="inputPassword4" placeholder="Password">
                    @error('pass')
                        <div class="mt-2 alert alert-danger">{{ $message }}</div>
                    @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPhone">Phone</label>
                    <input type="text" name="phone" class="form-control" id="inputPhone" placeholder="Phone">
                    @error('phone')
                        <div class="mt-2 alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="inputCMND">CMND</label>
                    <input type="text" name="cmnd" class="form-control" id="inputCMND" placeholder="CMND">
                    @error('cmnd')
                        <div class="mt-2 alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="inputBirthday">Birthday</label>
                    <input type="text" name="birthday" class="form-control" id="inputBirthday" placeholder="Birthday">
                    @error('birthday')
                        <div class="mt-2 alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Đăng ký</button>
                <a class="btn btn-primary" href="{{'./'}}">Đăng nhập</a>
            </form>
       </div>
</body>
</html>
